var app = require('express')();
const cheerio = require('cheerio');
const axios = require('axios');

app.get("/*", function(req,res) {
axios('https://api.github.com/repos/theabbie/theabbie.github.io/contents/client.html').then(function(x) {
const $ = cheerio.load(Buffer.from(x.data.content, 'base64'))
$(req.query.sel).html(req.query.body || $(req.query.sel).html())
$(req.query.sel).append(req.query.append || "")
axios({
  method: 'put',
  url: 'https://api.github.com/repos/theabbie/theabbie.github.io/contents/client.html',
  data: {
  "message": "my commit message",
  "committer": {
    "name": "abbie",
    "email": "abhishek7gg7@gmail.com"
  },
  "sha": x.data.sha,
  "content": Buffer.from($.html()).toString('base64')
},
headers: {
 "Content-Type" : "application/vnd.github.v3+json",
 "Authorization" : "token 374fb8fa5f6f82cdad527a8ba0845d76b55e3ccd"
}
}).then(function(m) {res.setHeader("Access-Control-Allow-Origin","*");res.end()})
})
})

app.listen(process.env.PORT);
